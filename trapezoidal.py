import numpy as np
import matplotlib.pyplot as plt

np.set_printoptions(linewidth=100, suppress=True)


class Trapezoidal:
    def __init__(self, simulator, epsilon, qd_obj=None):
        self.simulator=simulator
        if qd_obj is None:
            self.qd_obj=np.zeros(self.simulator.DOFs)
        else:
            self.qd_obj=qd_obj
        self.epsilon=epsilon
        
        
    def getMaxSign(self,qd,qdd,qddd_max,qd_obj):
        if qdd>=0:
            if qd_obj-qd >= (qdd**2/(2*qddd_max)):
                return 1
        else:
            if qd_obj-qd >= (qdd**2/-(2*qddd_max)):
                return 1
        return -1
        
    def getTrajectoryType(self,qd,qdd,qdd_max,qddd_max, qd_obj,sign):
        qdddToStopAcceleration=min(max((-qdd)/self.simulator.timestep,-qddd_max*sign),qddd_max*sign)
        if (np.abs(qd-qd_obj)<self.epsilon and np.abs(qdd)<self.epsilon) or (np.abs(qd-qd_obj)+((qdd**2)/np.abs(qdddToStopAcceleration))<self.epsilon):
            return -1
        elif (np.abs(qd_obj-qd) >= np.abs((2*qdd_max**2-qdd**2)/(2*qddd_max))):
            return 0
        else:
            return 1
        
    def isBetweenLimits(self,val,maxVal):
        if maxVal>0:
            return val <= maxVal
        else:
            return val >= maxVal
        
    def getT(self,trajectoryType,qd,qdd,qdd_max,qddd_max,sign,qd_obj):
        if trajectoryType==-1:
            return 0
        elif trajectoryType==0:
            return (qd_obj-qd)/qdd_max + (2*qdd_max**2-2*qdd*qdd_max+qdd**2)/(2*qddd_max*qdd_max)
        else:
            qdd_att=np.sqrt((2*qddd_max*(qd_obj-qd)+qdd**2)/2)*sign
            return (2*qdd_att-qdd)/qddd_max
        
    def getNextState(self,qd,qd_obj,trajectoryType,qddd_max,sign,qdd,qdd_max):
        if trajectoryType==-1:
            current_qdd_obj= 0

        elif trajectoryType==0:
            current_qdd_obj=qdd_max
        else:
            current_qdd_obj=np.sqrt((2*qddd_max*(qd_obj-qd)+qdd**2)/2)*sign #qdd_att
        return min(max((current_qdd_obj-qdd)/self.simulator.timestep,-qddd_max*sign),qddd_max*sign)
    
        
        
    def step(self,q,qd,qdd,oldTau,log=None,synchronise=False):
        
        # get sign of qdd_max and qddd_max
        vgetMaxSign=np.vectorize(self.getMaxSign)
        sign=vgetMaxSign(qd,qdd,self.simulator.qddd_max,self.qd_obj)
        current_qdd_max=np.max(np.array([np.abs(qdd),self.simulator.qdd_max]),axis=0)
        qdd_max=current_qdd_max * sign
        qddd_max=self.simulator.qddd_max * sign

        # get trajectory type
        vgetTrajectoryType=np.vectorize(self.getTrajectoryType)
        trajectoryType=vgetTrajectoryType(qd,qdd,qdd_max,qddd_max,self.qd_obj,sign) #0 if trapezoidal, 1 if triangular,-1 if the goal is achieved

        # compute braking time and synchronise if needed
        T=np.zeros(self.simulator.DOFs)
        for i in range(self.simulator.DOFs):
            T[i]=self.getT(trajectoryType[i],qd[i],qdd[i],qdd_max[i],qddd_max[i],sign[i],self.qd_obj[i])
        
        if synchronise:
            qdd_max=qdd_max*(T/np.max(T))
            qddd_max=qddd_max*(T/np.max(T))
            trajectoryType=vgetTrajectoryType(qd,qdd,qdd_max,qddd_max,self.qd_obj,sign) #0 if trapezoidal, 1 if triangular,-1 if the goal is achieved

        # compute next jerk and acceleration
        if log is not None:
            for dof in range(self.simulator.DOFs):
                log["T_DOF"+str(dof)].append(T[dof])
        vgetNextState=np.vectorize(self.getNextState)
        nextState=vgetNextState(qd,self.qd_obj,trajectoryType,qddd_max,sign,qdd,qdd_max)

        qddNext=qdd+nextState*self.simulator.timestep
        
        # compute desired tau and compute tauDot
        newTau=self.simulator.getTau(q,qd,qddNext)
        tauDot=(newTau-oldTau)/self.simulator.timestep
        
        # save logs
        if log is not None:
            for dof in range(self.simulator.DOFs):
                log["expectedqddd_DOF"+str(dof)].append(nextState[dof])
                log["expectedqdd_DOF"+str(dof)].append(qddNext[dof])
                log["trajectoryType_DOF"+str(dof)].append(trajectoryType[dof])
                log["sign_DOF"+str(dof)].append(sign[dof])
                log["tau_DOF"+str(dof)].append(newTau[dof])
                log["tauDot_DOF"+str(dof)].append(tauDot[dof])
        return tauDot
        
        
    """
    mode : 0 to save, 1 to display, 2 for both
    """
    def plotLog(self,log,keys,xlabel="Time(ms)",ylabel="",title="",mode=2,folder=""):
        for key in keys:
            plt.plot(log[key],label=key)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(title)
        plt.legend()
        if mode==0 or mode==2:
            if title!="":
                plt.savefig(folder+title+".png")
            else:
                plt.savefig(folder+keys[0]+".png")
        if mode==1 or mode==2:
            plt.show()
        plt.close()
    























