import gym
from gym import spaces
from pinocchioSimu import Simulator
from trapezoidal import Trapezoidal
from stable_baselines3.common.env_checker import check_env
import numpy as np
import os
import time
import pinocchio as pin

class GymSimu(gym.Env):
  """Custom Environment that follows gym interface"""
  metadata = {'render.modes': ['human']}

  def __init__(self,robotType="planar", delta=False, rewardShaping=False, curriculum=None, discrete=None, deltaRange=None, epsilon=0.01,timestep=0.001,maxSteps=2000, evaluator=False,excitingTrajectories=True,qd_obj=None,PCPAT=None,dynamicDelta=False,deltaOnDOFs=None):
    super(GymSimu, self).__init__()
    assert ((not delta) and deltaRange==None) or ((delta) and deltaRange!=None), 'If delta is false, deltaRange must be None, if delta is True, deltaRange must have a value'
    self.simulator=Simulator(robotType,timestep)
    self.delta=delta
    self.rewardShaping=rewardShaping
    self.discrete=discrete
    self.epsilon=epsilon
    self.evaluator=evaluator
    self.excitingTrajectories=excitingTrajectories
    self.PCPAT=PCPAT
    self.dynamicDelta=dynamicDelta
    if deltaOnDOFs is None:
        deltaOnDOFs=np.array([True]*self.simulator.DOFs)
    self.deltaOnDOFs=deltaOnDOFs
    self.countDelta=np.count_nonzero(deltaOnDOFs)
    if self.evaluator:
        self.lengths=[]
        self.stopped=[]
        self.getTestSet()
        self.currentTest=-1
        
    if qd_obj is None:
        self.qd_obj=np.zeros(self.simulator.DOFs)
    else:
        self.qd_obj=qd_obj
    
    self.trapezoidal=Trapezoidal(self.simulator, self.epsilon,self.qd_obj)

    if self.delta:
        if self.discrete is not None:
            self.discreteActions=np.linspace(-deltaRange,deltaRange,self.discrete)
            self.action_space = spaces.MultiDiscrete([self.discrete]*self.countDelta)
        else:
            self.action_space = spaces.Box(low=-deltaRange, high=deltaRange,shape=([self.countDelta]), dtype=np.float32)
    else:
        maxTaud=np.max(self.simulator.taud_max)
        self.action_space = spaces.Box(low=-maxTaud, high=maxTaud,shape=([self.simulator.DOFs]), dtype=np.float32)
    
    self.observation_space = spaces.Box(low=-21, high=21,shape=([self.simulator.DOFs*3]), dtype=np.float32)
    
    self.steps=0
    self.maxSteps=maxSteps
    if curriculum is not None:
        self.curriculum=True
        self.numberStepsCur=curriculum[0]
        self.newStepEvery=curriculum[1]
        self.currentStepCurriculum=1
        self.totalNumberSteps=0
    else:
        self.curriculum=False
        
    
  def steptaud(self, action):
    info=dict()
    #check if constraints are respected
    if not bool(np.all(np.abs(action)<=self.simulator.taud_max)) or not bool(np.all(np.abs(self.simulator.tau+self.simulator.timestep*action)<=self.simulator.tau_max)):
        print("invalid action")
        print(self.simulator.getState())
        print("Action : ",action)
        print(np.abs(action)<self.simulator.taud_max)
        print("New tau : ",self.simulator.tau+self.simulator.timestep*action)
        print(np.abs(self.simulator.tau+self.simulator.timestep*action)<self.simulator.tau_max)
        info["stopped"]=False
        if self.evaluator:
            self.stopped[-1].append(False)
        collisionReward=-(self.maxSteps-self.steps)*np.sum(np.abs(self.simulator.qd_max))
        return self.simulator.getState(),collisionReward,True,info
      
    self.steps+=1
    if self.evaluator:
        self.lengths[-1][self.currentTest]+=1
    
    self.simulator.step(action)
    self.qs=np.append(self.qs,[self.simulator.q],axis=0)
    observation=self.simulator.getState()
    
    #compute reward
    stopped=bool(np.all(np.abs(self.simulator.qd-self.qd_obj)<self.epsilon) and np.all(np.abs(self.simulator.qdd)<self.epsilon))
    terminal=stopped or self.steps>=self.maxSteps
    if terminal and self.evaluator:
        self.stopped[-1].append(stopped)
    if stopped:
        reward=100
    elif self.rewardShaping:
        reward=float(-1-np.sum(np.abs(self.simulator.qd)))
    else:
        reward=-1
    
    info["stopped"]=stopped
    return observation, reward, terminal, info

  def getActionOnAllDOFs(self,action):
      totAction=np.zeros(self.simulator.DOFs)
      count=0
      for i in range(self.deltaOnDOFs.shape[0]):
          if self.deltaOnDOFs[i]:
              totAction[i]=action[count]
              count+=1
          else:
              totAction[i]=0
      return totAction

  def computeDeltaFromAction(self, action):
      if self.discrete:
          action=self.discreteActions[action]
      action=self.getActionOnAllDOFs(action)
      if self.dynamicDelta=="diagonalInertiaMatrix":
          action=action*np.diagonal(pin.crba(self.simulator.robotObject.model, self.simulator.robotObject.data, self.simulator.q))
      #elif self.dynamicDelta=="deactivateLastDOF":
      #    action[-1]=0
      elif self.dynamicDelta=="tauMax":
          action=action*self.simulator.tau_max
      return action

  def step(self, action):
      
    if self.curriculum:
        self.totalNumberSteps+=1
        if self.totalNumberSteps%self.newStepEvery==0 and self.currentStepCurriculum<self.numberStepsCur:
            self.currentStepCurriculum+=1
            print("New curri step, max :", self.currentStepCurriculum*self.simulator.qd_max/self.numberStepsCur)
      
    action=self.computeDeltaFromAction(action)  
    
        
    if self.PCPAT is not None:
        action=np.where(np.abs(self.simulator.qd-self.qd_obj)<self.PCPAT,0,action)
        # if np.all(np.abs(self.simulator.qd-self.qd_obj)<self.PCPAT):
        #     action=np.zeros(self.simulator.DOFs)
        
    if self.delta:
        trapezoidalAction=self.trapezoidal.step(self.simulator.q,self.simulator.qd,self.simulator.qdd,self.simulator.tau)
        mini=np.max([-self.simulator.taud_max-trapezoidalAction,((-self.simulator.tau_max-self.simulator.tau)/self.simulator.timestep)-trapezoidalAction],axis=0)
        maxi=np.min([self.simulator.taud_max-trapezoidalAction,((self.simulator.tau_max-self.simulator.tau)/self.simulator.timestep)-trapezoidalAction],axis=0)
        action=np.amax([np.amin([action,maxi],axis=0),mini],axis=0)
        action+=trapezoidalAction
    
    observation, reward, terminal, info=self.steptaud(action)
    return observation, reward, terminal, info

  def stepTrap(self,respectConstraints=True):
    
    trapezoidalAction=self.trapezoidal.step(self.simulator.q,self.simulator.qd,self.simulator.qdd,self.simulator.tau)
    if respectConstraints:
        mini=np.max([-self.simulator.taud_max-trapezoidalAction,((-self.simulator.tau_max-self.simulator.tau)/self.simulator.timestep)-trapezoidalAction],axis=0)
        maxi=np.min([self.simulator.taud_max-trapezoidalAction,((self.simulator.tau_max-self.simulator.tau)/self.simulator.timestep)-trapezoidalAction],axis=0)
        action=np.amax([np.amin([np.zeros(self.simulator.DOFs),maxi],axis=0),mini],axis=0)
        action+=trapezoidalAction
    else:
        action=trapezoidalAction
    
    observation, reward, terminal, info=self.steptaud(action)
    return observation, reward, terminal, info
      
    

  def getTestSet(self,rewrite=False):
    if self.excitingTrajectories:
        if os.path.exists("./testSets/"+self.simulator.robotType+"TestSet.csv") and not rewrite:
            testSet=np.loadtxt("./testSets/"+self.simulator.robotType+"TestSet.csv", delimiter=",")
            self.testSet=testSet.reshape(testSet.shape[0],testSet.shape[1]//self.simulator.DOFs,self.simulator.DOFs)
        
        else:
            self.testSet=np.array([self.simulator.getRandomState() for _ in range(50)])
            np.savetxt("./testSets/"+self.simulator.robotType+"TestSet.csv", self.testSet.reshape(self.testSet.shape[0],-1), delimiter=",")
    else:
        if os.path.exists("./testSets/"+self.simulator.robotType+"TestSetRandom.csv") and not rewrite:
            testSet=np.loadtxt("./testSets/"+self.simulator.robotType+"TestSetRandom.csv", delimiter=",")
            self.testSet=testSet.reshape(testSet.shape[0],testSet.shape[1]//self.simulator.DOFs,self.simulator.DOFs)
        
        else:
            self.testSet=np.array([self.simulator.getRandomState(excitingTrajectories=False) for _ in range(50)])
            np.savetxt("./testSets/"+self.simulator.robotType+"TestSetRandom.csv", self.testSet.reshape(self.testSet.shape[0],-1), delimiter=",")

  def resetAtState(self,q,qd,qdd):
    self.steps=0
    self.simulator.setState(q,qd,qdd)
    self.qs=np.expand_dims(self.simulator.q,axis=0)
    return self.simulator.getState()

  def reset(self):
    self.steps=0
    if self.evaluator:
        self.currentTest+=1
        if self.currentTest>=len(self.testSet):
            self.currentTest=0
        if self.currentTest==0:
            self.lengths.append(np.zeros(len(self.testSet)))
            self.stopped.append([])
        [q,qd,qdd]=self.testSet[self.currentTest]
        self.simulator.setState(q,qd,qdd)
    else:
        self.simulator.setRandomState(excitingTrajectories=self.excitingTrajectories)
        if self.curriculum:
            while np.any(np.abs(self.simulator.qd)>self.currentStepCurriculum*self.simulator.qd_max/self.numberStepsCur):
                self.simulator.setRandomState(excitingTrajectories=self.excitingTrajectories)

    self.qs=np.expand_dims(self.simulator.q,axis=0)
    return self.simulator.getState()

  def visualizeLast(self,speed=1):
      self.simulator.visualize(self.qs,speed=speed)

  def render(self, mode='human'):
    print(self.simulator.q)
    
  def close (self):
    pass
    
if __name__ == '__main__':
    start=time.time()
    env=GymSimu()
    print()
    print("Create :",time.time()-start)
    print()
    print(check_env(env))
    start=time.time()
    env.reset()
    print("Reset :",time.time()-start)
    start=time.time()
    env.step(np.array([1,1]))
    print("Step :",time.time()-start)
