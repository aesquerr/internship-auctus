import gymSimu as gs
from stable_baselines3 import SAC, TD3, DDPG, PPO, A2C
from stable_baselines3.common.callbacks import EvalCallback
import pickle
import zipfile
import tools
import os
import numpy as np


def launchLearning_old(algo,robotName,path,rewardShaping,delta,timesteps,policy_kwargs,learning_rate, discrete=None, deltaRange=None,curriculum=None,excitingTrajectories=True,PCPAT=None,dynamicDelta=False,deltaOnDOFs=None):
    
    env=gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,curriculum=curriculum,discrete=discrete, deltaRange=deltaRange,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
    eval_env = gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,discrete=discrete, deltaRange=deltaRange, evaluator=True,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
    
    if algo=="SAC":
        model = SAC('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50, target_update_interval=50)
    elif algo=="TD3":
        model = TD3('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
    elif algo=="DDPG":
        model = DDPG('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
    elif algo=="PPO":
        model = PPO('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
    elif algo=="A2C":
        model = A2C('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
    eval_callback = EvalCallback(eval_env, best_model_save_path=path,n_eval_episodes=49,
                             log_path=path, eval_freq=50000,
                             deterministic=True, render=False)
    model.learn(total_timesteps=timesteps, callback=eval_callback)
    with open(path+"lenghtsEval.txt", "wb") as fp:
        pickle.dump(eval_env.lengths, fp)
    with open(path+"stoppedEval.txt", "wb") as fp:
        pickle.dump(eval_env.stopped, fp)
    with zipfile.ZipFile(path+'/evaluations.npz', 'r') as zip_ref:
        zip_ref.extractall(path+'/evaluations')
    model.save(path+"final_model")
    

def launchLearning(algo,robotName,path,rewardShaping,delta,timesteps,policy_kwargs,learning_rate, discrete=None, deltaRange=None,curriculum=None,excitingTrajectories=True,PCPAT=None,dynamicDelta=False,deltaOnDOFs=None,nbTrial=None):
    if nbTrial is None:
        env=gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,curriculum=curriculum,discrete=discrete, deltaRange=deltaRange,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
        eval_env = gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,discrete=discrete, deltaRange=deltaRange, evaluator=True,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
        
        if algo=="SAC":
            model = SAC('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50, target_update_interval=50)
        elif algo=="TD3":
            model = TD3('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
        elif algo=="DDPG":
            model = DDPG('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
        elif algo=="PPO":
            model = PPO('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
        elif algo=="A2C":
            model = A2C('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
        eval_callback = EvalCallback(eval_env, best_model_save_path=path,n_eval_episodes=49,
                                 log_path=path, eval_freq=50000,
                                 deterministic=True, render=False)
        model.learn(total_timesteps=timesteps, callback=eval_callback)
        with open(path+"lenghtsEval.txt", "wb") as fp:
            pickle.dump(eval_env.lengths, fp)
        with open(path+"stoppedEval.txt", "wb") as fp:
            pickle.dump(eval_env.stopped, fp)
        with zipfile.ZipFile(path+'/evaluations.npz', 'r') as zip_ref:
            zip_ref.extractall(path+'/evaluations')
        model.save(path+"final_model")
    else:
        reward=np.zeros(nbTrial)
        steps=np.zeros(nbTrial)
        energy=np.zeros(nbTrial)
        for i in range(nbTrial):
            print("TRIAL : "+str(i))
            pathTrial=path[:-1]+"_trial_"+str(i)+"/"
            
            env=gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,curriculum=curriculum,discrete=discrete, deltaRange=deltaRange,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
            
            if not os.path.isfile(pathTrial+"final_model.zip"):
                
                eval_env = gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,discrete=discrete, deltaRange=deltaRange, evaluator=True,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
                
                if algo=="SAC":
                    model = SAC('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50, target_update_interval=50)
                elif algo=="TD3":
                    model = TD3('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
                elif algo=="DDPG":
                    model = DDPG('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
                elif algo=="PPO":
                    model = PPO('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
                elif algo=="A2C":
                    model = A2C('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
                eval_callback = EvalCallback(eval_env, best_model_save_path=pathTrial,n_eval_episodes=49,
                                         log_path=pathTrial, eval_freq=50000,
                                         deterministic=True, render=False)
                model.learn(total_timesteps=timesteps, callback=eval_callback)
                with open(pathTrial+"lenghtsEval.txt", "wb") as fp:
                    pickle.dump(eval_env.lengths, fp)
                with open(pathTrial+"stoppedEval.txt", "wb") as fp:
                    pickle.dump(eval_env.stopped, fp)
                with zipfile.ZipFile(pathTrial+'/evaluations.npz', 'r') as zip_ref:
                    zip_ref.extractall(pathTrial+'/evaluations')
                model.save(pathTrial+"final_model")

            model=tools.loadModel(algo,pathTrial+"best_model")
            
            testSet=np.loadtxt("./testSets/"+robotName+"TestSetFinal.csv", delimiter=",")
            evaPolicyExt=tools.evaluator(env,model=model,testSet=testSet)
            evaPolicyExt.evaluatePolicy()
            reward[i]=np.mean(np.array(evaPolicyExt.logs["rew"]))
            steps[i]=np.mean(np.array(evaPolicyExt.logs["step"]))
            energy[i]=np.mean(np.array(evaPolicyExt.logs["energy"]))
        print("Reward")
        print(reward)
        print(np.mean(reward))
        print(np.std(reward))
        print("####")
        print("Step")
        print(steps)
        print(np.mean(steps))
        print(np.std(steps))
        print("Energy")
        print(energy)
        print(np.mean(energy))
        print(np.std(energy))
        
        
        
        
        
        
        
        
        
        