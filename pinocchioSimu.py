import pinocchio as pin
import numpy as np
import os
import time
from math import pi
import random

class Simulator:
    def __init__(self, robotType="planar",timestep=0.001,friction=1):
      if robotType=="planar":
          self.setPlanarRobot2DOF()
      elif robotType=="panda":
          self.setPandaRobot()
      elif robotType=="1DOF":
          self.set1DOFRobot()
      else:
          raise ValueError('robotType must be planar,panda or 1DOF')
      self.robotType=robotType
      self.DOFs=self.robotObject.model.nq
      self.timestep=timestep
      self.setExcitingTrajectories()
      self.friction=friction
      #CHANGER PARENTHESE FRICTION (le vrai c'est le dernier)
      
    def getState(self):
        return np.concatenate((self.q, self.qd, self.qdd), axis=None)
    
    def getTau(self,q,qd,qdd):
        b = pin.nle(self.robotObject.model,self.robotObject.data,q,qd)
        M = pin.crba(self.robotObject.model, self.robotObject.data, q)
        #return np.dot(M,qdd+self.friction*qd)+b
        return np.dot(M,qdd)+b+self.friction*qd
    
    def getEndEffectorPosition(self):
        # Perform the forward kinematics over the kinematic tree
        pin.forwardKinematics(self.robotObject.model,self.data,self.q)
        
        # Update Geometry models
        pin.updateGeometryPlacements(self.robotObject.model, self.data, self.robotObject.collision_model, self.collision_data)
        pin.updateGeometryPlacements(self.robotObject.model, self.data, self.robotObject.visual_model, self.visual_data)
        
        # print("\nJoint placements:")
        # for name, oMi in zip(self.robotObject.model.names, self.data.oMi):
        #     print(("{:<24} : {: .2f} {: .2f} {: .2f}"
        #           .format( name, *oMi.translation.T.flat )))
         
        # # Print out the placement of each collision geometry object
        # print("\nCollision object placements:")
        # for k, oMg in enumerate(self.collision_data.oMg):
        #     print(("{:d} : {: .2f} {: .2f} {: .2f}"
        #           .format( k, *oMg.translation.T.flat )))
         
        # # Print out the placement of each visual geometry object
        # print("\nVisual object placements:")
        # for k, oMg in enumerate(self.visual_data.oMg):
        #     print(("{:d} : {: .2f} {: .2f} {: .2f}"
        #           .format( k, *oMg.translation.T.flat )))
            
        for k, oMg in enumerate(self.collision_data.oMg):
            pass
            #print(*oMg.translation.T.flat)
            
        #print(*oMg.translation.T.flat)
        return oMg.translation.T.flat
    
    def setState(self,q,qd,qdd):
        self.q=q%(2*pi)
        self.qd=qd
        self.qdd=qdd
        self.tau=self.getTau(q,qd,qdd)
        self.kineticEnergy=self.computeKineticEnergy()
        
    def setRandomState(self,excitingTrajectories=True):
        [q,qd,qdd]=self.getRandomState(excitingTrajectories=excitingTrajectories)
        self.setState(q,qd,qdd)
        
    def getPandaRobot(self):
        #Build robot from urdf file
        srcabspath = os.path.abspath(os.path.dirname(__file__))
        path = 'robot_description'
        urdf_filename = 'panda.urdf'
        urdf_path = os.path.join(srcabspath, path, 'urdf', urdf_filename)
        mesh_dir = os.path.join(srcabspath, path, 'meshes')
        robot = pin.RobotWrapper.BuildFromURDF(urdf_path, mesh_dir)
        self.data, self.collision_data, self.visual_data  = pin.createDatas(robot.model, robot.collision_model, robot.visual_model)
        #robot, collision_model, visual_model = pin.buildModelsFromUrdf(urdf_path, mesh_dir)
        #data, collision_data, visual_data  = pin.createDatas(robot, collision_model, visual_model)
        
        #Joint space limits (https://frankaemika.github.io/docs/control_parameters.html#constants)
        tau_max=np.array([87, 87, 87, 87, 12, 12, 12])
        taud_max=np.ones(7)*1000*5
        q_min=np.array([-2.8973,-1.7628,-2.8973,-3.0718,-2.8973,-0.0175,-2.8973])
        q_max=np.array([2.8973,	1.7628,	2.8973,	-0.0698,	2.8973,	3.7525,	2.8973])
        qd_max=np.array([2.175, 2.175, 2.175, 2.175, 2.61, 2.61, 2.61])
        qdd_max=np.array([15, 7.5, 10, 12.5, 15, 20, 20])
        qddd_max=np.array([7500, 3750, 5000, 6250, 7500, 10000, 10000])/5
        
        return robot,tau_max,taud_max,q_min,q_max,qd_max,qdd_max,qddd_max

    def setPandaRobot(self):
        self.robotObject,self.tau_max,self.taud_max,self.q_min,self.q_max,self.qd_max,self.qdd_max,self.qddd_max = self.getPandaRobot()
        
    def setPlanarRobot2DOF(self):
        robot,tau_max,taud_max,q_min,q_max,qd_max,qdd_max,qddd_max = self.getPandaRobot()
        joints_to_lock = ['panda_joint1', 'panda_joint3', 'panda_joint5', 'panda_joint6', 'panda_joint7','panda_joint8']

        fixed_pose = pin.neutral(robot.model)
        self.robotObject  = robot.buildReducedRobot(list_of_joints_to_lock=joints_to_lock,
                                        reference_configuration=fixed_pose)
        self.tau_max,self.taud_max,self.q_min,self.q_max,self.qd_max,self.qdd_max,self.qddd_max= tau_max[[1, 3]],taud_max[[1, 3]],q_min[[1, 3]],q_max[[1, 3]],qd_max[[1, 3]],qdd_max[[1, 3]],qddd_max[[1, 3]]
        
    def set1DOFRobot(self):
        robot,tau_max,taud_max,q_min,q_max,qd_max,qdd_max,qddd_max = self.getPandaRobot()
        joints_to_lock = ['panda_joint1', 'panda_joint3', 'panda_joint4', 'panda_joint5', 'panda_joint6', 'panda_joint7','panda_joint8']

        fixed_pose = pin.neutral(robot.model)
        self.robotObject  = robot.buildReducedRobot(list_of_joints_to_lock=joints_to_lock,
                                        reference_configuration=fixed_pose)
        self.tau_max,self.taud_max,self.q_min,self.q_max,self.qd_max,self.qdd_max,self.qddd_max= tau_max[1:2],taud_max[1:2],q_min[1:2],q_max[1:2],qd_max[1:2],qdd_max[1:2],qddd_max[1:2]
        
        
    def accel(self,q, qd, tau):
        b = pin.nle(self.robotObject.model,self.robotObject.data,q,qd)
        M = pin.crba(self.robotObject.model, self.robotObject.data, q)
        #qdd = np.dot(np.linalg.inv(M),tau-b) - self.friction*qd
        qdd = np.dot(np.linalg.inv(M),tau-b- self.friction*qd) 
        return qdd
    
    def getNextState(self,taud):
        tau=np.amax([np.amin([self.tau+self.timestep*taud,self.tau_max],axis=0),-self.tau_max],axis=0)
        newqdd=self.accel(self.q, self.qd, tau)
        currentqddd=(newqdd-self.qdd)/self.timestep
        newqd=self.qd+self.qdd*self.timestep+0.5*currentqddd*self.timestep**2
        newqd=np.amax([np.amin([newqd,self.qd_max],axis=0),-self.qd_max],axis=0)
        newq=self.q+self.qd*self.timestep+0.5*self.qdd*self.timestep**2+(1/6)*currentqddd*self.timestep**3
        return tau, newq%(2*pi),newqd, newqdd
        
        
    def step(self,taud):
        
        #equations of motion
        self.tau=np.amax([np.amin([self.tau+self.timestep*taud,self.tau_max],axis=0),-self.tau_max],axis=0)
        newqdd=self.accel(self.q, self.qd, self.tau)
        currentqddd=(newqdd-self.qdd)/self.timestep
        newqd=self.qd+self.qdd*self.timestep+0.5*currentqddd*self.timestep**2
        newqd=np.amax([np.amin([newqd,self.qd_max],axis=0),-self.qd_max],axis=0)
        newq=self.q+self.qd*self.timestep+0.5*self.qdd*self.timestep**2+(1/6)*currentqddd*self.timestep**3
        self.qdd=newqdd
        self.qd=newqd
        self.q=newq%(2*pi)
        self.kineticEnergy=self.computeKineticEnergy()
        return np.concatenate((self.q, self.qd, self.qdd), axis=None)
        
        
    def visualize(self,qs,speed=1):
        self.robotObject.initViewer()
        self.robotObject.loadViewerModel()
        for q in qs:
            self.robotObject.display(q)
            time.sleep(self.timestep/speed)

    def getCoeffExistingTrajectories(self,N,qd_max=None):
        if qd_max is None:
            qd_max=self.qd_max
        coeffq=((self.q_max[:]-self.q_min[:])/2)/(sum([1/self.primes[:,n] for n in range(N)])*(2))
        coeffqd=self.qd_max/(N*(2))
        coeffqdd=self.qdd_max/(sum([self.primes[:,n] for n in range(N)])*(2))
        return np.expand_dims(np.min(np.array([coeffq,coeffqd,coeffqdd]),axis=0),axis=1)
    
    def computePrimes(self,N):
        nPrimes=N*self.DOFs
        primes=[]
        num=1
        while len(primes)<nPrimes:
            num+=1
            isPrime=True
            for i in range(2, num):
                if (num % i) == 0:
                    isPrime=False
                    break
            if isPrime:
                primes.append(num)
        random.shuffle(primes)
        if self.robotType=="panda":
            coeff=7
        else:
            coeff=1
        return np.reshape(np.array(primes),(self.DOFs,self.N))/coeff
        
    
    def getRandomState(self,newTraj=True,excitingTrajectories=True):
        if excitingTrajectories :
            if newTraj:
                self.setExcitingTrajectories()
            t=np.random.rand()*1000
            return self.computeStateExcitingTrajectories(t)
        else:
            q=np.random.uniform(low=self.q_min,high=self.q_max)
            qd=np.random.uniform(low=-self.qd_max,high=self.qd_max)
            qdd=np.random.uniform(low=-self.qdd_max,high=self.qdd_max)
            return np.array([q, qd, qdd])
        
    
    def setExcitingTrajectories(self,qd_max=None,N=3):
        if qd_max is None:
            qd_max=self.qd_max
        self.N=N
        self.w=1
        self.primes=self.computePrimes(self.N)
        c=self.getCoeffExistingTrajectories(self.N,qd_max)
        self.a=np.ones((self.DOFs,self.N))*c
        self.b=np.ones((self.DOFs,self.N))*c
        self.phaseBiasFirstTerm=np.random.rand(self.DOFs,self.N)*(2*pi)
        self.phaseBiasSecondTerm=np.random.rand(self.DOFs,self.N)*(2*pi)
        self.biasq=(self.q_max+self.q_min)/2
    
    def getExcitingTrajectories(self,duration,newTraj=True):
        if newTraj:
            self.setExcitingTrajectories()
        traj=np.array([self.computeStateExcitingTrajectories(t)[0] for t in np.linspace(0,duration,int(duration//self.timestep))])
        return traj
        
    def computeStateExcitingTrajectories(self,t):
        q=np.sum(np.array([(self.a[:,l]/(self.w*(self.primes[:,l])))*np.sin(self.w*(self.primes[:,l])*t+self.phaseBiasFirstTerm[:,l]) - \
                           (self.b[:,l]/(self.w*(self.primes[:,l])))*np.cos(self.w*(self.primes[:,l])*t+self.phaseBiasSecondTerm[:,l]) for l in range(self.N)]),axis=0)+self.biasq
        qd=np.sum(np.array([self.a[:,l]*np.cos(self.w*(self.primes[:,l])*t+self.phaseBiasFirstTerm[:,l]) + \
                            self.b[:,l]*np.sin(self.w*(self.primes[:,l])*t+self.phaseBiasSecondTerm[:,l]) for l in range(self.N)]),axis=0)
        qdd=np.sum(np.array([-self.a[:,l]*self.w*(self.primes[:,l])*np.sin(self.w*(self.primes[:,l])*t+self.phaseBiasFirstTerm[:,l]) + \
                             self.b[:,l]*self.w*(self.primes[:,l])*np.cos(self.w*(self.primes[:,l])*t+self.phaseBiasSecondTerm[:,l]) for l in range(self.N)]),axis=0)
        return np.array([q, qd, qdd])

    def computeKineticEnergy(self):
        M = pin.crba(self.robotObject.model, self.robotObject.data, self.q)
        return 0.5*self.qd.T.dot(M).dot(self.qd)

# if __name__ == '__main__':
#     simulator=Simulator("panda")
    
#     q=np.zeros(simulator.DOFs)
#     qd=np.zeros(simulator.DOFs)
#     qdd=np.zeros(simulator.DOFs)
#     simulator.setState(q,qd,qdd)
#     oldTau=simulator.tau
#     print("oldTau : ",oldTau)
#     qddd=np.array([0,3750,0,0,0,0,0])
#     qddNext=qdd+qddd*simulator.timestep
#     newTau=simulator.getTau(q,qd,qddNext)
#     tauDot=(newTau-oldTau)/simulator.timestep
#     print("newTau : ",newTau)
#     print("qddNext : ",qddNext)
#     print("taud :",tauDot)
