import torch

class MLP(torch.nn.Module):
        def __init__(self, input_size, hidden_size):
            super(MLP, self).__init__()
            self.input_size = input_size
            self.hidden_size  = hidden_size
            self.fc1 = torch.nn.Linear(self.input_size, self.hidden_size)
            self.relu1 = torch.nn.ReLU()
            self.fc2 = torch.nn.Linear(self.hidden_size, self.hidden_size)
            self.relu2 = torch.nn.ReLU()
            self.fc3 = torch.nn.Linear(self.hidden_size, 1)
            
        def forward(self, x):
            hidden1 = self.fc1(x)
            relu1 = self.relu1(hidden1)
            hidden2 = self.fc2(relu1)
            relu2 = self.relu2(hidden2)
            output = self.fc3(relu2)
            return output
        
        def evalModel(self,x_test,y_test):
            self.eval()
            y_pred = self(x_test)
            return self.criterion(y_pred.squeeze(), y_test) 
            
            

