from stable_baselines3 import SAC, TD3, DDPG, PPO, A2C
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from collections import defaultdict
import json
import gymSimu as gs
import time
import pinocchio as pin
import os
import shutil
import pandas as pd 
import copy
from sklearn.model_selection import train_test_split
import torch as th
from sklearn import preprocessing

mpl.rcParams['font.family'] = 'serif'

def loadModel(algo,nameFile):
    if algo=="SAC":
        model=SAC.load(nameFile)
    elif algo=="TD3":
        model=TD3.load(nameFile)
    elif algo=="DDPG":
        model=DDPG.load(nameFile)
    elif algo=="PPO":
        model=PPO.load(nameFile)
    elif algo=="A2C":
        model=A2C.load(nameFile)
    return model

def createDataSetTimePredictor(env,model,nbSamples,fileName):
    inputSize=env.simulator.DOFs*3
    initialState=[]
    modelBrakingTime=[]
    
    for sample in range(nbSamples):
        if sample%500==0:
            print(sample)
        obs = env.reset()
        initialState.append(obs)
        for i in range(env.maxSteps):
            action, _states = model.predict(obs, deterministic=True)
            obs, reward, terminal, info = env.step(action)
            if terminal:
                break
        modelBrakingTime.append(i)
   
    # dictionary of lists 
    dict = {'initialState': initialState, 'modelBrakingTime': modelBrakingTime} 
    df = pd.DataFrame(dict)
    #df=pd.DataFrame(df['initialState'].values.tolist())
    df[["input"+str(x) for x in range(inputSize)]]  = pd.DataFrame(df.initialState.tolist(), index= df.index)
    del df['initialState']
    # saving the dataframe
    df.to_csv(fileName, index=False)
    
def loadDataSetTimePredictor(fileName,numpy=False,scale=True):
    df = pd.read_csv(fileName)
    inputs=[]
    for column in df.columns:
        if column.startswith("input"):
            inputs.append(column)
    df['inputs'] = df[inputs].values.tolist()
    
    x_train, x_test, y_train, y_test = train_test_split(th.FloatTensor(df['inputs']), th.FloatTensor(df['modelBrakingTime']), test_size = 0.1, random_state = 0)
    if scale:
        scaler = preprocessing.StandardScaler().fit(x_train)
        x_train = th.from_numpy(scaler.transform(x_train)).float()
        x_test = th.from_numpy(scaler.transform(x_test)).float()
    if numpy:
        x_train, x_test, y_train, y_test = np.array(x_train),np.array(x_test),np.array(y_train),np.array(y_test)

    return x_train, x_test, y_train, y_test

def getPath(algo,robotName,rewardShaping,delta,it,policy_kwargs,lr,discrete,deltaRange,curriculum,excitingTrajectories,PCPAT,dynamicDelta,deltaOnDOFs=None):
    path="./policiesData/"+robotName+"/"+algo+"/"
    if rewardShaping:
        path+="RS_"
    if delta:
        path+="delta_"
    lrstr=str(lr).replace('.', '_')
    curriculumstr="_curriculum"+str(curriculum).replace(',', '_').replace('[', '').replace(']', '')
    layerstr="_layer"+str(policy_kwargs["net_arch"]).replace(',', '_').replace('[', '').replace(']', '')
    if deltaOnDOFs is None:
        deltaOnDOFsstr=""
    else:
        deltaOnDOFsstr="_deltaOnDOFs"+str(deltaOnDOFs).replace(',', '_').replace('[', '').replace(']', '')
    
    path+="discrete"+str(discrete)+"_"+str(it)+"it_lr"+lrstr+"_range"+str(deltaRange)+layerstr+curriculumstr+"_excitingTrajectories"+str(excitingTrajectories)+"_PCPAT"+str(PCPAT)+"_dynamicDelta"+str(dynamicDelta)+deltaOnDOFsstr+"/"

    return path

# def analyseActor(model, qd_max, qdd_max, granularity=20, folder=""):
#     DOFs=qd_max.shape[0]
#     q=np.zeros(DOFs)
#     predictions=np.zeros((DOFs, granularity, granularity))
#     for dof in range(DOFs):
#         qd=np.zeros(DOFs)
#         qdd=np.zeros(DOFs)
#         qds_n=np.linspace(-qd_max[dof],qd_max[dof],granularity)
#         qdds_n=np.linspace(-qdd_max[dof],qdd_max[dof],granularity)
#         for i in range(qds_n):
#             qd[dof]=qds_n[i]
#             for j in range(qdds_n):
#                 qdd[dof]=qdds_n[i]
#                 state=np.concatenate((q, qd, qdd), axis=None)
#                 action, _states = model.predict(state, deterministic=True)
#                 predictions[dof,i,j]=action[dof]
#         plt.hist2d(qds_n,qdds_n,predictions[dof])
#         plt.savefig(folder+"prediction"+str(dof)+".png")
#         plt.close()
       
def visualizeTrap(env):
    isTrap=env.delta
    env.delta=True
    obs = env.reset()
    for i in range(env.maxSteps):
        obs, reward, terminal, info = env.step(np.zeros(env.simulator.DOFs))
        if terminal:
            break
    env.delta=isTrap
    for _ in range(1):
        if i!=0:
            time.sleep(2)
        env.visualizeLast()  
        
def visualizePolicy(env,model,nbRepeatEach=1,nbEpisodes=10):
    for _ in range(nbEpisodes):
        obs = env.reset()
        for i in range(env.maxSteps):
            action, _states = model.predict(obs, deterministic=True)
            obs, reward, terminal, info = env.step(action)
            if terminal:
                break
        for i in range(nbRepeatEach):
            if i!=0:
                time.sleep(2)
            env.visualizeLast()
        time.sleep(2)
        
def analyseLearning(algo,robotName,path,rewardShaping,delta,discrete=None,deltaRange=None,excitingTrajectories=True,PCPAT=None,dynamicDelta=False,deltaOnDOFs=None):
    if not os.path.exists(path+'analyseResults'):
        os.makedirs(path+'analyseResults')
    else:
        shutil.rmtree(path+'analyseResults')           
        os.makedirs(path+'analyseResults')
    
    
    trap_env = gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=False, evaluator=False,excitingTrajectories=excitingTrajectories)
    trap_env.getTestSet()
    eva=evaluator(trap_env,testSet=trap_env.testSet)
    eva.evaluateTrap()
    
    logsResults=dict()
    
    timesteps,reward,lengths,stopped=getLearningLogs(path)
    print("Last :",np.mean(reward[-1]))
    print(reward[-1])
    #print(np.mean(reward,axis=1))
    print("Max :",np.max(np.mean(reward,axis=1)))
    print(reward[np.argmax(np.mean(reward,axis=1))])
    print("Trap : ",np.mean(eva.rewardsTrap))
    print(eva.rewardsTrap)
    plt.plot(timesteps, np.mean(reward,axis=1))
    plt.plot(timesteps,[np.mean(eva.rewardsTrap) for _ in reward])
    plt.savefig(path+'analyseResults/reward.png')
    plt.show()
    plt.close()
    plt.plot(timesteps, np.where(np.mean(reward,axis=1)<np.mean(eva.rewardsTrap)-50 , np.mean(eva.rewardsTrap)-50,np.mean(reward,axis=1)))
    plt.plot(timesteps,[np.mean(eva.rewardsTrap) for _ in reward])
    plt.savefig(path+'analyseResults/reward_modified_figure.png')
    plt.show()
    plt.close()
    
    
    testSet=np.loadtxt("./testSets/"+robotName+"TestSetFinal.csv", delimiter=",")
    testSetRandom=np.loadtxt("./testSets/"+robotName+"TestSetFinalRandom.csv", delimiter=",")
    trap_env = gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=False, evaluator=False,excitingTrajectories=excitingTrajectories)
    evaTrapEXT=evaluator(trap_env,testSet=testSet)
    evaTrapEXT.evaluateTrap()
    
    timesteps,reward,lengths,stopped=getLearningLogs(path)
    #print("Last reward :", reward[-1])
    #print(evaTrapEXT.rewardsTrap)
    print("Test Set exciting traj :")
    print("Trap on big testSet (energy):",np.mean(np.array(evaTrapEXT.energiesTrap)))
    print("Trap on big testSet (reward):",np.mean(np.array(evaTrapEXT.rewardsTrap)))
    print("Trap on big testSet (steps):",np.mean(np.array(evaTrapEXT.stepNumbersTrap)))
    print("Trap on big testSet (steps per DOF):",np.mean(evaTrapEXT.stoppingTimesPerDOF,axis=0))
    print("Trap on big testSet (last stopping):",evaTrapEXT.lastStoppingDOFs/np.sum(evaTrapEXT.lastStoppingDOFs))
    logsResults["Trap exciting traj reward"]=np.mean(np.array(evaTrapEXT.rewardsTrap))
    logsResults["Trap exciting traj steps"]=np.mean(np.array(evaTrapEXT.stepNumbersTrap))
    
    evaTrapR=evaluator(trap_env,testSet=testSetRandom)
    evaTrapR.evaluateTrap()
    
    print("Test Set Random :")
    print("Trap on big testSet (energy):",np.mean(np.array(evaTrapR.energiesTrap)))
    print("Trap on big testSet (reward):",np.mean(np.array(evaTrapR.rewardsTrap)))
    print("Trap on big testSet (steps):",np.mean(np.array(evaTrapR.stepNumbersTrap)))
    print("Trap on big testSet (steps per DOF):",np.mean(evaTrapR.stoppingTimesPerDOF,axis=0))
    print("Trap on big testSet (last stopping):",evaTrapR.lastStoppingDOFs/np.sum(evaTrapR.lastStoppingDOFs))
    logsResults["Trap random reward"]=np.mean(np.array(evaTrapR.rewardsTrap))
    logsResults["Trap random steps"]=np.mean(np.array(evaTrapR.stepNumbersTrap))
    
        
    
    # lenghtsModified=np.where(stopped,lengths,2000)
    # plt.plot(timesteps, np.mean(lenghtsModified,axis=1))
    # plt.show()
    
    model=loadModel(algo,path+"best_model")
    env=gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta, discrete=discrete,deltaRange=deltaRange,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
    
    
    evaPolicyExt=evaluator(env,model=model,testSet=testSet)
    evaPolicyExt.evaluatePolicy()
    
    print("Test Set exciting traj :")
    print("Max on big testSet (energy):",np.mean(np.array(evaPolicyExt.logs["energy"])))
    print("Max on big testSet (reward):",np.mean(np.array(evaPolicyExt.logs["rew"])))
    print("Max on big testSet (steps):",np.mean(np.array(evaPolicyExt.logs["step"])))
    print(len(evaPolicyExt.logs["step"]))
    print(np.array(evaPolicyExt.logs["step"]).shape)
    print(np.array(evaTrapEXT.stepNumbersTrap).shape)
    
    print("Number worst braking time : ",np.count_nonzero(np.array(evaPolicyExt.logs["step"][0])>np.array(evaTrapEXT.stepNumbersTrap)))
    longer=np.array(evaPolicyExt.logs["step"][0])>np.array(evaTrapEXT.stepNumbersTrap)
    print("time loss : ",np.array(evaPolicyExt.logs["step"][0])[longer]-np.array(evaTrapEXT.stepNumbersTrap)[longer])
    print("MAX time loss : ",np.max(np.array(evaPolicyExt.logs["step"][0])[longer]-np.array(evaTrapEXT.stepNumbersTrap)[longer]))
    
    print("Max on big testSet (steps per DOF):",np.mean(evaPolicyExt.logs["stoppingTimesPerDOF"][0],axis=0))
    print("Max on big testSet (last stopping):",evaPolicyExt.logs["lastStoppingDOFs"]/np.sum(evaPolicyExt.logs["lastStoppingDOFs"]))
    logsResults["Model exciting traj reward"]=np.mean(np.array(evaPolicyExt.logs["rew"]))
    logsResults["Model exciting traj steps"]=np.mean(np.array(evaPolicyExt.logs["step"]))
    
    
    evaPolicyR=evaluator(env,model=model,testSet=testSetRandom)
    evaPolicyR.evaluatePolicy()
    print("Test Set Random :")
    print("Max on big testSet (energy):",np.mean(np.array(evaPolicyR.logs["energy"])))
    print("Max on big testSet (reward):",np.mean(np.array(evaPolicyR.logs["rew"])))
    print("Max on big testSet (steps):",np.mean(np.array(evaPolicyR.logs["step"])))
    print("Max on big testSet (steps per DOF):",np.mean(evaPolicyR.logs["stoppingTimesPerDOF"][0],axis=0))
    print("Max on big testSet (last stopping):",evaPolicyR.logs["lastStoppingDOFs"]/np.sum(evaPolicyR.logs["lastStoppingDOFs"]))
    logsResults["Model random reward"]=np.mean(np.array(evaPolicyR.logs["rew"]))
    logsResults["Model random steps"]=np.mean(np.array(evaPolicyR.logs["step"]))
    
    plt.plot(evaPolicyExt.logs["kineticEnergy"],np.array(evaPolicyExt.logs["step"])-np.array(evaTrapEXT.stepNumbersTrap), 'bo')
    plt.xlabel("Kinetic energy $(J)$")
    plt.ylabel("Time difference (ms)")
    plt.title("to do")
    if path is not None:
        plt.savefig(path+'analyseResults/kineticTimeDifExt.png',bbox_inches='tight',dpi=500)
    plt.show()
    plt.close()
    print("Ext kinetic Energy average : ",np.mean(evaPolicyExt.logs["kineticEnergy"]))
    print("higher braking time ratio : ",((np.array(evaPolicyExt.logs["step"])-np.array(evaTrapEXT.stepNumbersTrap))>0).sum()/len(evaTrapEXT.stepNumbersTrap))
    
    plt.plot(evaPolicyR.logs["kineticEnergy"],np.array(evaPolicyR.logs["step"])-np.array(evaTrapR.stepNumbersTrap), 'bo')
    plt.xlabel("Kinetic energy $(J)$")
    plt.ylabel("Time difference (ms)")
    plt.title("to doo")
    if path is not None:
        plt.savefig(path+'analyseResults/kineticTimeDifR.png',bbox_inches='tight',dpi=500)
    plt.show()
    plt.close()
    print("RAND kinetic Energy average : ",np.mean(evaPolicyR.logs["kineticEnergy"]))
    print("higher braking time ratio : ",((np.array(evaPolicyR.logs["step"])-np.array(evaTrapR.stepNumbersTrap))>0).sum()/len(evaTrapR.stepNumbersTrap))
    
    json.dump( logsResults, open(path+'analyseResults/logsResults.txt', 'w') )
    
    compareModelWithTrap(env,model)
    return np.mean(np.array(evaPolicyR.logs["rew"])),np.mean(np.array(evaPolicyR.logs["step"]))
    
        
def compareModelWithTrap(env,model,path=None,displayActionNothing=False,displayTrapModel=False,model2=None,env2=None,initState=None):
    if initState is not None:
        obs = env.resetAtState(initState[0],initState[1],initState[2])
    else:
        obs = env.reset()
    print(obs)
    
    kineticEnergy=env.simulator.computeKineticEnergy()
    
    
    modelq=[env.simulator.q]
    modelqd=[env.simulator.qd]
    modelqdd=[env.simulator.qdd]
    modelqddd=[]
    modeltau=[env.simulator.tau]
    modelKineticEnergy=[env.simulator.kineticEnergy]
    if env.delta:
        modelaction=[]
    modeltaud=[]
        
    if model != None:
        trapModelq=[env.simulator.q]
        trapModelqd=[env.simulator.qd]
        trapModelqdd=[env.simulator.qdd]
        trapModeltau=[env.simulator.tau]
        trapModeltaud=[]
        for i in range(env.maxSteps):
            action, _states = model.predict(obs, deterministic=True)
            if env.delta:
                obs2=np.reshape(env.simulator.getState(),(3,env.simulator.DOFs))
                actionTrap=env.trapezoidal.step(obs2[0],obs2[1],obs2[2],env.simulator.tau)
                #print(actionTrap)
            #print(action)
            # print()
            # print(obs)
            # print("###   :   ",action)
            # print("###   :   ",actionTrap)
            obs, reward, terminal, info = env.step(action)
            modelq.append(env.simulator.q)
            modelqd.append(env.simulator.qd)
            modelqdd.append(env.simulator.qdd)
            modelqddd.append((modelqdd[-1]-modelqdd[-2])/env.simulator.timestep)
            modeltau.append(env.simulator.tau)
            modelKineticEnergy.append(env.simulator.kineticEnergy)
            withoutModel=env.simulator.getNextState(actionTrap)
            trapModelq.append(withoutModel[1])
            trapModelqd.append(withoutModel[2])
            trapModelqdd.append(withoutModel[3])
            trapModeltau.append(withoutModel[0])
            trapModeltaud.append(actionTrap)
            if env.delta:
                modelaction.append(env.computeDeltaFromAction(action))
                modeltaud.append(env.computeDeltaFromAction(action)+actionTrap)
                # if env.discrete is not None:
                #     #print(actionTrap)
                #     modeltaud.append(env.computeDeltaFromDiscreteAction(action)+actionTrap)
                # else:
                #     modeltaud.append(action+actionTrap)
            else:
                modeltaud.append(action)
            if terminal:
                break
        print("Kinetic energy :",kineticEnergy)
            
        print("MODEL")
        print("Length : ",i)
        print("Sum tau : ",np.sum(np.abs(np.array(modeltau))))
        print("Sum tau square : ",np.sum(np.array(modeltau)**2))
        print("Stopped",info["stopped"])
    
    
    
    
    if model2 is not None:
        env2.resetAtState(np.array(modelq[0]),np.array(modelqd[0]),np.array(modelqdd[0]))
        model2q=[env2.simulator.q]
        model2qd=[env2.simulator.qd]
        model2qdd=[env2.simulator.qdd]
        model2qddd=[]
        model2tau=[env2.simulator.tau]
        model2KineticEnergy=[env2.simulator.kineticEnergy]
        if env2.delta:
            model2action=[]
        model2taud=[]
    
            
        for i in range(env2.maxSteps):
            action, _states = model2.predict(obs, deterministic=True)
            if env2.delta:
                obs2=np.reshape(env2.simulator.getState(),(3,env2.simulator.DOFs))
                actionTrap=env2.trapezoidal.step(obs2[0],obs2[1],obs2[2],env2.simulator.tau)
                #print(actionTrap)
            #print(action)
            # print()
            # print(obs)
            # print("###   :   ",action)
            # print("###   :   ",actionTrap)
            obs, reward, terminal, info = env2.step(action)
            model2q.append(env2.simulator.q)
            model2qd.append(env2.simulator.qd)
            model2qdd.append(env2.simulator.qdd)
            model2qddd.append((model2qdd[-1]-model2qdd[-2])/env2.simulator.timestep)
            model2tau.append(env2.simulator.tau)
            model2KineticEnergy.append(env2.simulator.kineticEnergy)
            if env2.delta:
                model2action.append(env2.computeDeltaFromAction(action))
                model2taud.append(env2.computeDeltaFromAction(action)+actionTrap)
            else:
                model2taud.append(action)
            if terminal:
                break
        print("Kinetic energy :",kineticEnergy)
            
        print("model2")
        print("Length : ",i)
        print("Sum tau : ",np.sum(np.abs(np.array(model2tau))))
        print("Sum tau square : ",np.sum(np.array(model2tau)**2))
        print("Stopped",info["stopped"])
        
    
    env.resetAtState(np.array(modelq[0]),np.array(modelqd[0]),np.array(modelqdd[0]))
    tmpDelta,tmpDiscrete,tmpDynamicDelta=env.delta,env.discrete,env.dynamicDelta
    env.delta=True
    env.discrete=None
    env.dynamicDelta=False
    trapq=[modelq[0]]
    trapqd=[modelqd[0]]
    trapqdd=[modelqdd[0]]
    trapqddd=[]
    traptau=[modeltau[0]]
    trapKineticEnergy=[env.simulator.kineticEnergy]
    traptaud=[]
    
    for i in range(env.maxSteps):
        obs=np.reshape(env.simulator.getState(),(3,env.simulator.DOFs))
        actionRecord=env.trapezoidal.step(obs[0],obs[1],obs[2],env.simulator.tau)
        action=np.zeros(env.simulator.DOFs)
        obs, reward, terminal, info = env.step(action)
        trapq.append(env.simulator.q)
        trapqd.append(env.simulator.qd)
        trapqdd.append(env.simulator.qdd)
        trapqddd.append((trapqdd[-1]-trapqdd[-2])/env.simulator.timestep)
        traptau.append(env.simulator.tau)
        trapKineticEnergy.append(env.simulator.kineticEnergy)
        traptaud.append(actionRecord)
        if terminal:
            break
        
    env.delta,env.discrete,env.dynamicDelta=tmpDelta,tmpDiscrete,tmpDynamicDelta
    print("TRAP")
    print("Length : ",i)
    print("Sum tau : ",np.sum(np.abs(np.array(traptau))))
    print("Sum tau square : ",np.sum(np.array(traptau)**2))
    print("Stopped",info["stopped"])
    
    
    
    env.resetAtState(np.array(modelq[0]),np.array(modelqd[0]),np.array(modelqdd[0]))
    nothingq=[modelq[0]]
    nothingqd=[modelqd[0]]
    nothingqdd=[modelqdd[0]]
    nothingtau=[modeltau[0]]
    nothingtaud=[]
    
    for i in range(env.maxSteps):
        obs=np.reshape(env.simulator.getState(),(3,env.simulator.DOFs))
        tautarget = pin.nle(env.simulator.robotObject.model,env.simulator.robotObject.data,obs[0],obs[1])
        action=np.min(np.array([np.max(np.array([(tautarget-env.simulator.tau)/env.simulator.timestep,-env.simulator.taud_max]),axis=0),env.simulator.taud_max]),axis=0)
        #action=env.trapezoidal.step(obs[0],obs[1],obs[2],env.simulator.tau)
        obs, reward, terminal, info = env.steptaud(action)
        nothingq.append(env.simulator.q)
        nothingqd.append(env.simulator.qd)
        nothingqdd.append(env.simulator.qdd)
        nothingtau.append(env.simulator.tau)
        nothingtaud.append(action)
        if terminal:
            break
        
    print("NOTHING")
    print("Length : ",i)
    print("Stopped",info["stopped"])
    
   
    modelq=np.array(modelq)
    modelqd=np.array(modelqd)
    modelqdd=np.array(modelqdd)
    modelqddd=np.array(modelqddd)
    modeltau=np.array(modeltau)
    modelKineticEnergy=np.array(modelKineticEnergy)
    print(env.delta)
    if env.delta:
        modelaction=np.array(modelaction)
    modeltaud=np.array(modeltaud)
    
    if model != None:
        trapModelq=np.array(trapModelq)
        trapModelqd=np.array(trapModelqd)
        trapModelqdd=np.array(trapModelqdd)
        trapModeltau=np.array(trapModeltau)
        trapModeltaud=np.array(trapModeltaud)
    
    if model2 is not None:
        model2q=np.array(model2q)
        model2qd=np.array(model2qd)
        model2qdd=np.array(model2qdd)
        model2qddd=np.array(model2qddd)
        model2tau=np.array(model2tau)
        model2KineticEnergy=np.array(model2KineticEnergy)
        if env.delta:
            model2action=np.array(model2action)
        model2taud=np.array(model2taud)
    
    trapq=np.array(trapq)
    trapqd=np.array(trapqd)
    trapqdd=np.array(trapqdd)
    trapqddd=np.array(trapqddd)
    traptau=np.array(traptau)
    trapKineticEnergy=np.array(trapKineticEnergy)
    traptaud=np.array(traptaud)
    
    nothingq=np.array(nothingq)
    nothingqd=np.array(nothingqd)
    nothingqdd=np.array(nothingqdd)
    nothingtau=np.array(nothingtau)
    nothingtaud=np.array(nothingtaud)
    
    for i in range(env.simulator.DOFs):
        xMaxLength=range(max(len(modelq[:,i]),len(trapq[:,i])))
        print()
        print("#### DOF :",i+1)
        print()
        
        
        # if displayActionNothing:
        #     plot([range(len(modelq[:,i])), range(len(trapq[:,i])), range(len(trapModelq[:,i])), range(len(nothingq[:,i]))],
        #          [modelq[:,i],trapq[:,i],trapModelq[:,i],nothingq[:,i]],["modelq","trapq","trapModelq","nothingq"],"Position q",path=path)
        # else:
        #     plot([range(len(modelq[:,i])), range(len(trapq[:,i])), range(len(trapModelq[:,i]))],
        #          [modelq[:,i],trapq[:,i],trapModelq[:,i]],["modelq","trapq","trapModelq"],"Position q",path=path)
        if model != None:
            plt.plot(range(len(modelq[:,i])),modelq[:,i],label="$PAT+\Delta \dot{\\tau}$")
        plt.plot(range(len(trapq[:,i])),trapq[:,i],label="$PAT$",color='C1')
        if model2 is not None:
            plt.plot(range(len(model2q[:,i])),model2q[:,i],label="$PAT+\Delta \dot{\\tau} (3)$")
        if displayTrapModel:
            plt.plot(range(len(trapModelq[:,i])),trapModelq[:,i],label="trapModelq")
        if displayActionNothing:
            plt.plot(range(len(nothingq[:,i])),nothingq[:,i],label="nothingq")
        plt.xlabel("Time (ms)")
        plt.ylabel("Position $q$ $(rad)$")
        plt.title("Position $q$")
        plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
        plt.grid()
        if path is not None:
            plt.savefig(path+'analyseResults/Position q'+str(i)+'.png',bbox_inches='tight',dpi=500)
        plt.show()
        plt.close()
        
        plt.plot(xMaxLength,[env.simulator.qd_max[i] for _ in xMaxLength],'r--',label="Limits")
        plt.plot(xMaxLength,[-env.simulator.qd_max[i] for _ in xMaxLength],'r--')
        if model != None:
            plt.plot(range(len(modelq[:,i])),modelqd[:,i],label="$PAT+\Delta \dot{\\tau}$")
        plt.plot(range(len(trapq[:,i])),trapqd[:,i],label="$PAT$",color='C1')
        if model2 is not None:
            plt.plot(range(len(model2q[:,i])),model2qd[:,i],label="$PAT+\Delta \dot{\\tau} (3)$")
        if displayTrapModel:
            plt.plot(range(len(trapModelq[:,i])),trapModelqd[:,i],label="trapModelqd")
        if displayActionNothing:
            plt.plot(range(len(nothingq[:,i])),nothingqd[:,i],label="nothingqd")
        plt.xlabel("Time (ms)")
        plt.ylabel("Velocity $\dot{q}$ $(rad/s)$")
        plt.title("Velocity $\dot{q}$")
        plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
        plt.grid()
        if path is not None:
            plt.savefig(path+'analyseResults/Velocity qd'+str(i)+'.png',bbox_inches='tight',dpi=500)
        plt.show()
        plt.close()
        
        plt.plot(xMaxLength,[env.simulator.qdd_max[i] for _ in xMaxLength],'r--',label="Limits")
        plt.plot(xMaxLength,[-env.simulator.qdd_max[i] for _ in xMaxLength],'r--')
        if model != None:
            plt.plot(range(len(modelq[:,i])),modelqdd[:,i],label="$PAT+\Delta \dot{\\tau}$")
        plt.plot(range(len(trapq[:,i])),trapqdd[:,i],label="$PAT$",color='C1')
        if model2 is not None:
            plt.plot(range(len(model2q[:,i])),model2qdd[:,i],label="$PAT+\Delta \dot{\\tau} (3)$")
        if displayTrapModel:
            plt.plot(range(len(trapModelq[:,i])),trapModelqdd[:,i],label="trapModelqdd")
        if displayActionNothing:
            plt.plot(range(len(nothingq[:,i])),nothingqdd[:,i],label="nothingqdd")
        plt.xlabel("Time (ms)")
        plt.ylabel("Acceleration $\ddot{q}$ $(rad/s^2)$")
        plt.title("Acceleration $\ddot{q}$")
        plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
        plt.grid()
        if path is not None:
            plt.savefig(path+'analyseResults/Acceleration qdd'+str(i)+'.png',bbox_inches='tight',dpi=500)
        plt.show()
        plt.close()
        
        plt.plot(xMaxLength,[env.simulator.qddd_max[i] for _ in xMaxLength],'r--',label="Limits")
        plt.plot(xMaxLength,[-env.simulator.qddd_max[i] for _ in xMaxLength],'r--')
        if model != None:
            plt.plot(range(len(modelqddd[:,i])),modelqddd[:,i],label="$PAT+\Delta \dot{\\tau}$")
        plt.plot(range(len(trapqddd[:,i])),trapqddd[:,i],label="$PAT$",color='C1')
        if model2 is not None:
            plt.plot(range(len(model2qddd[:,i])),model2qddd[:,i],label="$PAT+\Delta \dot{\\tau} (3)$")
        plt.xlabel("Time (ms)")
        plt.ylabel("Jerk $\dddot{q}$ $(rad/s^3)$")
        plt.title("Jerk $\dddot{q}$")
        plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
        plt.grid()
        if path is not None:
            plt.savefig(path+'analyseResults/Jerk qddd'+str(i)+'.png',bbox_inches='tight',dpi=500)
        plt.show()
        plt.close()
        
        plt.plot(xMaxLength,[env.simulator.tau_max[i] for _ in xMaxLength],'r--',label="Limits")
        plt.plot(xMaxLength,[-env.simulator.tau_max[i] for _ in xMaxLength],'r--')
        if model != None:
            plt.plot(range(len(modelq[:,i])),modeltau[:,i],label="$PAT+\Delta \dot{\\tau}$")
        plt.plot(range(len(trapq[:,i])),traptau[:,i],label="$PAT$",color='C1')
        if model2 is not None:
            plt.plot(range(len(model2q[:,i])),model2tau[:,i],label="$PAT+\Delta \dot{\\tau} (3)$")
        if displayTrapModel:
            plt.plot(range(len(trapModelq[:,i])),trapModeltau[:,i],label="trapModeltau")
        if displayActionNothing:
            plt.plot(range(len(nothingq[:,i])),nothingtau[:,i],label="nothingtau")
        plt.xlabel("Time (ms)")
        plt.ylabel("Torque $\\tau$ $(N.m)$")
        plt.title("Torque $\\tau$")
        plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
        plt.grid()
        limGraph=plt.xlim()
        if path is not None:
            plt.savefig(path+'analyseResults/Tau'+str(i)+'.png',bbox_inches='tight',dpi=500)
        plt.show()
        plt.close()
        
        if model != None:
            if env.delta:
                plt.plot(range(len(modelaction[:,i])),modelaction[:,i],label="$PAT+\Delta \dot{\\tau}$")
                if model2 is not None:
                    plt.plot(range(len(model2action[:,i])),model2action[:,i],label="$PAT+\Delta \dot{\\tau}$ (3)",color='g')
                plt.xlabel("Time (ms)")
                plt.ylabel("Action $\Delta\dot{\\tau}$ $(N.m/s)$")
                plt.title("Action $\Delta\dot{\\tau}$")
                plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
                plt.xlim(limGraph)
                plt.grid()
                if path is not None:
                    plt.savefig(path+'analyseResults/Action model'+str(i)+'.png',bbox_inches='tight',dpi=500)
                plt.show()
                plt.close()
            
        plt.plot(xMaxLength,[env.simulator.taud_max[i] for _ in xMaxLength],'r--',label="Limits")
        plt.plot(xMaxLength,[-env.simulator.taud_max[i] for _ in xMaxLength],'r--')
        if model != None:
            plt.plot(range(len(modeltaud[:,i])),modeltaud[:,i],label="$PAT+\Delta \dot{\\tau}$")
        plt.plot(range(len(traptaud[:,i])),traptaud[:,i],label="$PAT$",color='C1')
        if model2 is not None:
            plt.plot(range(len(model2taud[:,i])),model2taud[:,i],label="$PAT+\Delta \dot{\\tau} (3)$")
        if displayTrapModel:
            plt.plot(range(len(trapModeltaud[:,i])),trapModeltaud[:,i],label="trapModeltaud")
        if displayActionNothing:
            plt.plot(range(len(nothingtaud[:,i])),nothingtaud[:,i],label="nothingtaud")
        plt.xlabel("Time (ms)")
        plt.ylabel("Derivative of the torque $\dot{\\tau}$ $(N.m/s)$")
        plt.title("Derivative of the torque $\dot{\\tau}$")
        plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
        plt.grid()
        if path is not None:
            plt.savefig(path+'analyseResults/Taud'+str(i)+'.png',bbox_inches='tight',dpi=500)
        plt.show()
        plt.close()
     
    if model != None:
        plt.plot(range(len(modelKineticEnergy)),modelKineticEnergy,label="$PAT+\Delta \dot{\\tau}$")
    plt.plot(range(len(trapKineticEnergy)),trapKineticEnergy,label="$PAT$",color='C1')
    if model2 is not None:
        plt.plot(range(len(model2KineticEnergy)),model2KineticEnergy,label="$PAT+\Delta \dot{\\tau} (3)$")
    plt.xlabel("Time (ms)")
    plt.ylabel("Kinetic energy $(J)$")
    plt.title("Kinetic energy")
    plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
    plt.grid()
    if path is not None:
        plt.savefig(path+'analyseResults/Kinetic energy.png',bbox_inches='tight',dpi=500)
    plt.show()
    plt.close()
        
    #print(modeltau[0:10])
    #print(traptau[0:10])
    
    #same graph
    titles=["Velocity $\dot{q}$","Acceleration $\ddot{q}$","Jerk $\dddot{q}$",
            "Torque $\\tau$","Derivative of the torque $\dot{\\tau}$", "Action $\Delta\dot{\\tau}$"]
    ylabels=["Velocity $\dot{q}$ $[rad/s]$","Acceleration $\ddot{q}$ $[rad/s^2]$","Jerk $\dddot{q}$ $[rad/s^3]$",
             "Torque $\\tau$ $[N.m]$","Derivative of the torque $\dot{\\tau}$ $[N.m/s]$","Action $\Delta\dot{\\tau}$ $[N.m/s]$"]
    #ylabels=["$\dot{q}$ $[rad/s]$","$\ddot{q}$ $[rad/s^2]$","$\dddot{q}$ $[rad/s^3]$",
    #         "$\\tau$ $[N.m]$","$\dot{\\tau}$ $[N.m/s]$","$\Delta\dot{\\tau}$ $[N.m/s]$"]
    limits=[env.simulator.qd_max,env.simulator.qdd_max,env.simulator.qddd_max,
                env.simulator.tau_max,env.simulator.taud_max]
    valuesModel=[modelqd,modelqdd,modelqddd,
                     modeltau,modeltaud,modelaction]
    #print(valuesModel[0].shape)
    valuesTAP=[trapqd,trapqdd,trapqddd,
                   traptau,traptaud]
    
    #linesLabels=["Limits","$PAT+\Delta \dot{\\tau}$","$PAT$"]
    
    linewidth=2
    fontsize=13
    
    for i in range(env.simulator.DOFs):
        fig, ax = plt.subplots(2, 3)
        for j in range(6):
            #print(j//3)
            #print(j%3)
            #print(j)
            subfig=ax[j//3,j%3]
            if j!=5:
                subfig.plot(xMaxLength,[limits[j][i] for _ in xMaxLength],'r--',label="Limits")
                subfig.plot(xMaxLength,[-limits[j][i] for _ in xMaxLength],'r--')
            if j==2 or j==4 or j==5:
                xValuesModel=range(1,len(modelq[:,i]))
                xValuesTrap=range(1,len(trapq[:,i]))
            else:
                xValuesModel=range(len(modelq[:,i]))
                xValuesTrap=range(len(trapq[:,i]))
            #print(xValuesModel)
            #print(valuesModel[j][:,i])
            subfig.plot(xValuesModel,valuesModel[j][:,i],label="$TAP+\Delta \dot{\\tau}$",linewidth=linewidth)
            if j!=5:
                subfig.plot(xValuesTrap,valuesTAP[j][:,i],label="$TAP$",color='C1',linewidth=linewidth)
            if j>=3:
                subfig.set_xlabel("Time (ms)",fontsize=fontsize)
            subfig.set_ylabel(ylabels[j],fontsize=fontsize)
            #subfig.set_title(titles[j],fontsize=fontsize)
            #subfig.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
            subfig.grid()
            subfig.set_xlim(limGraph)
          
            handles, labels = plt.gca().get_legend_handles_labels()


          
        handles_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
        handles, labels = [sum(lol, []) for lol in zip(*handles_labels)]
        by_label = dict(zip(labels, handles))
        fig.legend(by_label.values(), by_label.keys(),bbox_to_anchor=(0.91, 0.78), loc='lower left', borderaxespad=0.,fontsize=fontsize-2)
        name = path+'analyseResults/allGraph'+str(i)+'_smallVersion.png'
        fig.set_size_inches(20, 8)
        fig.subplots_adjust(hspace=0.15,wspace=0.25)
        fig.savefig(name, dpi=300, bbox_inches='tight')
        
        plt.show()
        plt.close()
            
        """plt.plot(xMaxLength,[env.simulator.tau_max[i] for _ in xMaxLength],'r--',label="Limits")
            plt.plot(xMaxLength,[-env.simulator.tau_max[i] for _ in xMaxLength],'r--')
            if model != None:
                plt.plot(range(len(modelq[:,i])),modeltau[:,i],label="$PAT+\Delta \dot{\\tau}$")
            plt.plot(range(len(trapq[:,i])),traptau[:,i],label="$PAT$",color='C1')
            if model2 is not None:
                plt.plot(range(len(model2q[:,i])),model2tau[:,i],label="$PAT+\Delta \dot{\\tau} (3)$")
            if displayTrapModel:
                plt.plot(range(len(trapModelq[:,i])),trapModeltau[:,i],label="trapModeltau")
            if displayActionNothing:
                plt.plot(range(len(nothingq[:,i])),nothingtau[:,i],label="nothingtau")
            plt.xlabel("Time (ms)")
            plt.ylabel("$\\tau$ $(N.m)$")
            plt.title("Torque $\\tau$")
            plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
            plt.grid()
            limGraph=plt.xlim()
            if path is not None:
                plt.savefig(path+'analyseResults/Tau'+str(i)+'.png',bbox_inches='tight',dpi=500)
            plt.show()
            plt.close()"""
    
def plotCompare(xs,ys,labels,xLabel,yLabel,title,path=None):
    for i in range(len(xs)):
        plt.plot(xs[i],ys[i],label=labels[i])
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.title(title)
    plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
    if path is not None:
        plt.savefig(path+'analyseResults/'+title+'.png',bbox_inches='tight',dpi=500)
    plt.show()
    plt.close()
        
    
def getLearningLogs(path):
    reward=np.load(path+"evaluations/results.npy")
    timesteps=np.load(path+"evaluations/timesteps.npy")
    # with open(path+"lenghtsEval.txt", "rb") as fp:
    #     lengths=np.array(pickle.load(fp))
    lengths=None
    # with open(path+"stoppedEval.txt", "rb") as fp:
    #     stopped=pickle.load(fp)
    stopped=None
    return timesteps,reward,lengths,stopped

class evaluator:
    def __init__(self,env,model=None,testSet=None):
        self.logs=defaultdict(list)
        self.env=env
        self.model=model
        if testSet is None:
            self.testSet=self.env.testSet[:-1]
        else:
            self.testSet=testSet
            
    def evaluateTrap(self):
        isTrap=self.env.delta
        self.env.delta=True
        self.rewardsTrap=[]
        self.stepNumbersTrap=[]
        self.stoppedsTrap=[]
        self.energiesTrap=[]
        self.kineticEnergyTrap=[]
        self.stoppingTimesPerDOF=[]
        self.lastStoppingDOFs=np.zeros(self.env.simulator.DOFs)
        for initialState in self.testSet:
            reward,stepNumber,stopped,energy,kineticEnergy,stoppingTimePerDOF,lastStoppingDOF=self.evaluatePolicyOnEnv(initialState,onlyTrap=True)
            self.rewardsTrap.append(reward)
            self.stepNumbersTrap.append(stepNumber)
            self.stoppedsTrap.append(stopped)
            self.energiesTrap.append(energy)
            self.kineticEnergyTrap.append(kineticEnergy)
            self.stoppingTimesPerDOF.append(stoppingTimePerDOF)
            self.lastStoppingDOFs[lastStoppingDOF]+=1
        self.env.delta=isTrap
        
    def evaluatePolicyOnEnv(self,initialState,onlyTrap=False):
        obs=np.reshape(initialState,(3,self.env.simulator.DOFs))
        obs=self.env.resetAtState(obs[0],obs[1],obs[2])
        kineticEnergy=self.env.simulator.computeKineticEnergy()
        totalEnergy=np.abs(self.env.simulator.tau)
        totalReward=0
        stoppingTimePerDOF=np.array([self.env.maxSteps]*self.env.simulator.DOFs)
        for step in range(self.env.maxSteps):
            if onlyTrap:
                # print()
                # print("Obs : ",obs[0]," -- ",obs[1]," -- ",obs[2])
                # print("Tau : ",self.env.simulator.tau)
                action=np.zeros(self.env.simulator.DOFs)
                # print("Action : ",action)
            else:
                action, _states = self.model.predict(obs, deterministic=True)
  
            obs, reward, terminal, info = self.env.step(action)
            totalEnergy+=np.abs(self.env.simulator.tau)
            totalReward+=reward
            stoppingTimePerDOF=np.where(np.logical_and(np.logical_and(np.abs(self.env.simulator.qd-self.env.qd_obj)<self.env.epsilon,np.abs(self.env.simulator.qdd)<self.env.epsilon),stoppingTimePerDOF==self.env.maxSteps),step,stoppingTimePerDOF)
            if terminal:
                #print(stoppingTimePerDOF)
                lastStoppingDOF=np.argmax(stoppingTimePerDOF)
                break
                
        return totalReward,step,info["stopped"],np.sum(totalEnergy),kineticEnergy,stoppingTimePerDOF,lastStoppingDOF
    
    def evaluatePolicy(self):
        # print("logs:")
        # print(self.logs)
        # start=time.time()
        rewards=[]
        stepNumbers=[]
        stoppeds=[]
        energies=[]
        kineticEnergies=[]
        stoppingTimesPerDOF=[]
        lastStoppingDOFs=np.zeros(self.env.simulator.DOFs)
        for initialState in self.testSet:
            reward,stepNumber,stopped,energy,kineticEnergy,stoppingTimePerDOF,lastStoppingDOF=self.evaluatePolicyOnEnv(initialState)
            rewards.append(reward)
            stepNumbers.append(stepNumber)
            stoppeds.append(stopped)
            energies.append(energy)
            kineticEnergies.append(kineticEnergy)
            stoppingTimesPerDOF.append(stoppingTimePerDOF)
            lastStoppingDOFs[lastStoppingDOF]+=1
        self.logs["rew"].append(rewards)
        self.logs["step"].append(stepNumbers)
        self.logs["stop"].append(stoppeds)
        self.logs["energy"].append(energies)
        self.logs["kineticEnergy"].append(kineticEnergies)
        self.logs["stoppingTimesPerDOF"].append(np.array(stoppingTimesPerDOF))
        self.logs["lastStoppingDOFs"].append(lastStoppingDOFs)
        
    def saveLogs(self,folder="./policiesData/SAC/"):
        print(self.logs["rew"])
        print("saving...")
        print(np.mean(self.logs["rew"],axis=1))
        plt.plot(np.mean(self.logs["rew"],axis=1))
        plt.savefig(folder+"mean"+".png")
        plt.close()
    
    
    